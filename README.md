# Monk+Gitlab 5 minute app

To deploy:

1. See https://docs.monk.io/monk-in-10.html for the first time setup of Monk (done only once, takes up to 10 min)
2. Once you have monk and a cluster running:
    ```
    docker build . -t <yourname>/myapp
    docker push <yourname>/myapp:latest
    monk load MANIFEST
    monk run -t <yourtag> myapp/system
    ```
3. Enjoy
