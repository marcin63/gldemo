var express = require("express");
var router = express.Router();

/* GET home page. */
router.get("/", function (req, res, next) {
    res.render("index", { title: "Monk demo Node.js app", env: process.env });
});

module.exports = router;
